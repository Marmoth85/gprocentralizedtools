package gproCentralizedTools;

import java.util.InputMismatchException;
import java.util.Scanner;

import bigRacePSC.RaceURL;
import gproFinancesAnalyser.FinancesAnalyser;
//import javafx.application.Application;
//import javafx.event.ActionEvent;
//import javafx.event.EventHandler;
//import javafx.scene.Scene;
//import javafx.scene.control.Button;
//import javafx.scene.layout.StackPane;
//import javafx.stage.Stage;

public class ToolsRoot/* extends Application*/{
	
	private static int STAFF_TOOL_CODE = 3;
	private static int QUALIFS_TOOL_CODE = 1;
	private static int FINANCES_TOOL_CODE = 4;
	private static int BIG_RACE_TOOL_CODE = 2;
	private static int EXIT_CODE = 5;

	
	public ToolsRoot(){
		// TODO Auto-generated constructor stub
	}
	
	
	private void loadTextMenu() throws Exception {
		
		String menuChoices = "Quel outil souhaitez-vous utiliser?\n";
		
		for (int i = 1; i <= EXIT_CODE; i++) {
			if (i == STAFF_TOOL_CODE) {
				menuChoices += STAFF_TOOL_CODE + " - L'outil basé sur la récupération des niveaux du staff,\n";
			}
			if (i == QUALIFS_TOOL_CODE) {
				menuChoices += QUALIFS_TOOL_CODE + " - L'outil basé sur la récupération des temps de qualifications,\n";
			}
			if (i == EXIT_CODE) {
				menuChoices += EXIT_CODE + " - Quitter l'application.\n";
			}
			if (i == FINANCES_TOOL_CODE) {
				menuChoices += FINANCES_TOOL_CODE + " - L'outil facilitant la production d'un bilan financier de la saison,\n";
			}
			if (i == BIG_RACE_TOOL_CODE) {
				menuChoices += BIG_RACE_TOOL_CODE + " - L'outil d'extraction de données pour la Big Race,\n";
			}
		}
		menuChoices += "Veuillez entrer votre choix : ";
		
		int userChoice = 0;
		boolean work = true;
		
		final Scanner sc = new Scanner (System.in);
		
		while (work) {
			System.out.println(menuChoices);
			
			try {
				userChoice = sc.nextInt();
				
				if (userChoice == STAFF_TOOL_CODE) {
					
					System.out.println("Vous avez choisi l'outil récupérant les niveaux de staff.\n");
					
				} else if (userChoice == QUALIFS_TOOL_CODE) {
					
					System.out.println("Vous avez choisi l'outil de récupération des temps de qualifications.\n");
					
				} else if (userChoice == EXIT_CODE) {
					
					work = false;
					System.out.println("Vous avez choisi de quitter l'application.\n");
					
				} else if (userChoice == FINANCES_TOOL_CODE) {
					
					System.out.println("Vous avez choisi l'outil d'analyse financière d'une saison.\n");
					final FinancesAnalyser analyser = new FinancesAnalyser();
					analyser.process();
					
				} else if (userChoice == BIG_RACE_TOOL_CODE) {
					
					System.out.println("Vous avez choisi l'outil d'extraction de données pour les compétitions de la PSC.\n");
					final RaceURL psc = new RaceURL();
					psc.setScanner(sc);
					psc.processText();
					
				}
				else {
					System.err.println("Votre réponse ne figure pas dans les options proposées : veuillez corriger votre réponse.\n");
				}
			}
			catch (InputMismatchException e) {
				System.err.println("Votre réponse n'est pas un nombre entier comme attendu, veuillez corriger votre réponse.\n");
				userChoice = 0;
				sc.nextLine();
			}
		}
		sc.close();
	}

	
	public static void main(String[] args) throws Exception {
		final ToolsRoot tr = new ToolsRoot();
		tr.loadTextMenu();
//		launch(args);
	}


//	@Override
//	public void start(Stage primaryStage) throws Exception {
//		Button btn = new Button();
//        btn.setText("Say 'Hello World'");
//        btn.setOnAction(new EventHandler<ActionEvent>() {
// 
//            @Override
//            public void handle(ActionEvent event) {
//                System.out.println("Hello World!");
//            }
//        });
//        
//        StackPane root = new StackPane();
//        root.getChildren().add(btn);
//        
//        Scene scene = new Scene(root, 300, 250);
//
//        primaryStage.setTitle("Hello World!");
//        primaryStage.setScene(scene);
//        primaryStage.show();
//	}
}